import time
import threading
from threading import Thread


# def sleepMe(i):
#     print("Поток %i засыпает на 5 секунд.\n" % i)
#     time.sleep(5)
#     print("Поток %i сейчас проснулся.\n" % i)


def sleepMe(i):
    print("Поток %s засыпает на 5 секунд.\n" % threading.current_thread())
    time.sleep(5)
    print("Поток %s сейчас проснулся.\n" % threading.current_thread())


# for i in range(10):
#     th = Thread(target=sleepMe, args=(i, ))
#     th.start()
# 


for i in range(10):
    th = Thread(target=sleepMe, args=(i, ))
    th.start()
    #print("Запущено потоков: %i." % threading.active_count())


def delayed():
    print("Вывод через 5 секунд!")


thread = threading.Timer(3, delayed)
thread.start()


print(threading.main_thread())
print(threading.enumerate())


for _ in range(10):
    print('hello')
















