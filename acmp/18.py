fi = open('input.txt', 'r')
st = fi.read()
fi.close()

import os
st = int(st)
if st < 0 or st > 1000:
	os.system('exit')
s_ = 1
for i in range(1, st + 1):
	s_ *= i

fo = open('output.txt', 'w')
fo.write(str(s_))
fo.close()
