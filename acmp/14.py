fi = open('input.txt', 'r')
st = fi.read()
fi.close()
st = [int(n) for n in st.split(' ')]
s = st[0] * st[1]
while not st[0] == st[1]:
	if st[0] > st[1]:
		st[0] -= st[1]
	elif st[0] < st[1]:
		st[1] -= st[0]
print(s//st[0])
fo = open('output.txt', 'w')
fo.write(str(s//st[0]))
fo.close()

