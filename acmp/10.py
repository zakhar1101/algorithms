import sys

fi = open('input.txt', 'r')
l = fi.read()
fi.close()

l = l.split(' ')
l = [int(n) for n in l]
if not l[0] == 0 and abs(l[0]) <= 32768 and abs(l[1]) <= 32768 and abs(l[2]) <= 32768 and abs(l[3]) <= 32768:
	a = l[0]
	b = l[1]
	c = l[2]
	d = l[3]
	for x in range(-100, 101):
		if a*x**3 + b*x**2 + c*x + d == 0:
			print(x)
else:
	sys.exit()