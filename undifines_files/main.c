#include "my_funcs.h"


int main()
{
	printf("Type number: ");
	int n;
	int* arr;

	scanf("%d", &n);

	arr = (int*)malloc(sizeof(int) * n);
	
	int i, j, k;
	for (i = 0; i < n; i++) {
		arr[i] = rand() % 20;
	}
	show_arr(arr, n);

	sort_selection(arr, n);

	sort_by_insert(arr, n);




	show_arr(arr, n);
	free(arr);
	return 0;
}
