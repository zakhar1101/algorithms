import numpy as np

def appearance( intervals ):
    lesson = intervals['lesson']
    pupil  = intervals['pupil']
    tutor  = intervals['tutor']
    lesson_time = lesson[1] - lesson[0]

    arr_u1 = []
    arr_u2 = []
    
    # заполенение массивов координат ученика и учителя, а также обрезка по краям (0-3600)
    #print('ученик')
    for i in range(1, len(pupil), 2):
        x1 = pupil[i - 1] - lesson[0]
        x2 = pupil[i] - lesson[0]

        if x1 <= 0:
            arr_u1.append(0)
        elif x1 > 3600:
            arr_u1.append(3600)
        else:
            arr_u1.append(x1)

        if x2 <= 0:
            arr_u1.append(0)
        elif x2 > 3600:
            arr_u1.append(3600)
        else:
            arr_u1.append(x2)
        #print('{0:5d} {1:5d} {2:5d}'.format(x2 - x1, x1, x2))

    #print('Учитель')
    for i in range(1, len(tutor), 2):
        y1 = tutor[i - 1] - lesson[0]
        y2 = tutor[i] - lesson[0]

        if y1 <= 0:
            arr_u2.append(0)
        elif y1 > 3600:
            arr_u2.append(3600)
        else:
            arr_u2.append(y1)

        if y2 <= 0:
            arr_u2.append(0)
        elif y2 > 3600:
            arr_u2.append(3600)
        else:
            arr_u2.append(y2)
        #print('{0:5d} {1:5d} {2:5d}'.format(y2 - y1, y1, y2))

    
    # сортировка и удаление дублиикатов
    arr_u1 = sorted(set(arr_u1))
    arr_u2 = sorted(set(arr_u2))


    # минимальное справа
    if arr_u1[-1::][0] > arr_u2[-1::][0]:
        arr_u1[len(arr_u1) - 1] = arr_u2[len(arr_u2) - 1]
    if arr_u1[-1::][0] < arr_u2[-1::][0]:
        arr_u2[len(arr_u2) - 1] = arr_u1[len(arr_u1) - 1]

    acc = 0
    summ = 0
    while True:
        a = arr_u2[0] if arr_u1[0] < arr_u2[0] else arr_u1[0]

        if arr_u1[1] < arr_u2[1]:
            b = arr_u1[1]
            arr_u1 = arr_u1[2::]
        elif arr_u1[1] > arr_u2[1]:
            b = arr_u2[1]
            arr_u2 = arr_u2[2::]
        else:
            summ += arr_u2[-1::][0] - a
            print(summ)
            return summ
            break

        summ += b - a
        #print(arr_u1)
        #print(arr_u2)

            




    
tests = [
{ 
    'data': { 
        'lesson': [ 1594663200, 1594666800 ], 
        'pupil':  [ 1594663340, 1594663389, 1594663390, 1594663395, 1594663396, 1594666472 ],
        'tutor':  [ 1594663290, 1594663430, 1594663443, 1594666473 ] },
    'answer':  3117 },
{  
    'data': { 
        'lesson': [ 1594692000, 1594695600 ],
        'pupil': [ 1594692033, 1594696347 ],
        'tutor': [ 1594692017, 1594692066, 1594692068, 1594696341 ] },
    'answer':  3565 }
]

#{
#    'data': { 
#        'lesson': [ 1594702800, 1594706400 ],
#        'pupil':  [ 1594702789, 1594704500, 1594702807, 1594704542, 1594704512, 1594704513, 1594704564, 1594705150, 1594704581, 1594704582, 1594704734, 1594705009, 1594705095, 1594705096, 1594705106, 1594706480, 1594705158, 1594705773, 1594705849, 1594706480, 1594706500, 1594706875, 1594706502, 1594706503, 1594706524, 1594706524, 1594706579, 1594706641 ],
#        'tutor':  [ 1594700035, 1594700364, 1594702749, 1594705148, 1594705149, 1594706463 ] },
#    'answer':  3577 },

if __name__ ==  '__main__':
    for i, test in enumerate(tests):
        print('#', i)
        test_answer = appearance(test['data'])
        print()
        assert  test_answer == test[ 'answer' ], f'Error on test case { i }, got { test_answer }, expected { test[ "answer" ] }'



