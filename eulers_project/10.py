#решето Эрстофена
n=2*10**6
#n=100
arr=[1 for _ in range(n)]
arr[0]=0


arr[1]=0
i=2
while i*i<=n:
    if arr[i]==1:
        for j in range(i*i,n,i):
            arr[j]=0
    i+=1

s=0
for i in range(n):
    if i%10==0:
        print()
    if arr[i]==1:
        s+=i
    print(arr[i],end=' ')
print()
print(s)
